﻿using LabJob1;
using System;

namespace LabJob1


{
    /// <summary> 
    ///     Реализует методы взаимодействия с пользователем для получения данных о геометрических фигурах для дальнейшего рассчёта.
    /// </summary>

    public class UserInputInterface
    {
        /// <summary>
        ///     Метод для взаимодействия с пользователем с целью получения радиуса круга.
        /// </summary>
        /// <remarks>
        /// <para>
        ///     Метод получает от пользователя данные о радиусе круга, ожидая тип double. 
        ///     При срабатывании исключения System.FormatException происходит его обработка и возврат к получению данных от пользователя.
        ///     При срабатывании прочих исключений происходит, вывод информации пользователю и throw.
        /// </para>
        /// </remarks>
        /// <returns>
        ///     Радиус круга типа double, введённный пользователем.
        /// </returns>

        // Метод для взаимодействия с пользователем с целью получения радиуса круга.
        public static double GetCircleRadius()
        {
            // локальная переменная circle_radius для радиуса круга
            double circle_radius;
            try
            {
                Console.Write($"\nВведите размер радиуса круга: ");
                circle_radius = Convert.ToDouble(Console.ReadLine().Replace('.', '.'));
            }
            // обработка пользовательской ошибки ввода данных о фигуре
            catch (System.FormatException)
            {
                Console.Write($"\nВведённое значение некорректно. Необходимо ввести числовое значение\n");
                return GetCircleRadius();
            }
            // обработка прочих исключений
            catch (Exception ex)
            {
                Console.Write($"\nПроизошло необработанное исключение {ex}, {ex.Message}. попробуйте перезапустить программу\n");
                throw;
            }
            return circle_radius;
        }


        /// <summary>
        ///     Метод взаимодействия с пользователем для получения длины стороны квадрата.
        /// </summary>
        /// <remarks>
        /// <para>
        ///     Метод получает от пользователя данные о длине стороны квадрата, ожидая тип double. 
        ///     При срабатывании исключения System.FormatException происходит его обработка и возврат к получению данных от пользователя.
        ///     При срабатывании прочих исключений происходит вывод информации пользователю и throw.
        /// </para>
        /// </remarks>
        /// <returns>
        ///     Длина стороны квадрата типа double, введённный пользователем.
        /// </returns>

        // Метод для взаимодействия с пользователем с целью получения длины стороны квадрата.
        public static double GetSquareSide()
        {
            // локальная переменная square_side для радиуса круга
            double square_side;
            try
            {
                Console.Write($"Введите длину стороны квадрата: ");
                square_side = Convert.ToDouble(Console.ReadLine());
            }
            // обработка пользовательской ошибки ввода данных о фигуре
            catch (System.FormatException)
            {
                Console.Write($"\nВведённое значение некорректно. Необходимо ввести числовое значение\n");
                return GetSquareSide();
            }
            // обработка прочих исключений
            catch (Exception ex)
            {
                Console.Write($"\nПроизошло необработанное исключение {ex}, {ex.Message}. попробуйте перезапустить программу\n");
                throw;
            }
            return square_side;
        }
    }
}


    /// <summary> 
    ///     Реализует методы математического расчёта площадей треугольника, квадрата. Сравнение площадей этих фигур.
    /// </summary>
    public class Calculation
{

    /// <summary>
    ///     Метод для расчёта площади квадрата
    /// </summary>
    /// <returns>
    ///     Площадь квадрата типа double
    /// </returns>
    /// <param name="square_side"> 
    ///     Параметр типа double означающий длину стороны квадрата.
    /// </param>
    public static double CalculateSquareArea(double square_side)
    {
        // возвращает площадь квадрата на основании параметра square_side, означающего длину стороны квадрата
        return Math.Pow(square_side, 2);
    }

    /// <summary>
    ///     Метод для расчёта площади квадрата
    /// </summary>
    /// <returns>
    ///     Площадь круга типа double 
    /// </returns>
    /// <param name="circle_radius"> 
    ///     Параметр типа double означающий радиус круга.
    /// </param>
    public static double CalculateCircleArea(double circle_radius)
    {
        // возвращает площадь круга на основании параметра circle_radius, означающего радиус круга
        return Math.PI * Math.Pow(circle_radius, 2);
    }


    /// <summary>
    ///     Метод для сравнения площадей фигур
    /// </summary>
    /// <remarks>
    ///     Производит сравнение площадей указанных фигур. Имеет два входных параметра типа double. О результатах информирует с помощью Console.WriteLine.
    /// </remarks>
    /// <param name="square_area"> 
    ///     Параметр типа double означающий длину стороны квадрата.
    /// </param>
    /// <param name="circle_area"> 
    ///     Параметр типа double означающий радиус круга.
    /// </param>
    public static string Compare(double square_area, double circle_area)
    {
        string out_message = "";
        
        if (square_area > circle_area)
        {
            out_message = $"\nРезультат расчёта:\n Площадь квадрата больше площади круга на {square_area - circle_area}";
        }
        else if (square_area < circle_area)
        {
            out_message = $"\nРезультат расчёта:\n Площадь круга больше площади квадрата на {circle_area - square_area}";
        }
        else if (square_area == circle_area)
        {
            out_message = $"\nРезультат расчёта:\n Площади круга и квадрата одинаковы";
        }
        Console.WriteLine(out_message);
        return out_message;
    }
}



/// <summary> 
///     Вызов реализованных методов классов для запуска программы в контексте расчёта площадей фигур
/// </summary>
public class Program
{
    static void Main(string[] args)
    {
        // украшение, не влияющее на логику
        Console.WriteLine($"\nПриветствую!\nПрограмма из серии 'Разветвления', вариант №1. \n\nВыполнена студентом группы ИСТб-21-2: \nПогораевым Никитой Петровичем\n");
        while (true)
        {
            // украшение, не влияющее на логику
            Console.Write("Что-бы приступить к расчётам нажмите любую клавишу...");
            Console.ReadKey();
            Console.Clear();

            // получение данных от пользователя
            double input_circle = UserInputInterface.GetCircleRadius(); // получение данных о радиусе круге
            double input_square = UserInputInterface.GetSquareSide(); // получение данных о длине стороны квадрата


            // рассчёт площадей фигур
            double square_area = Calculation.CalculateSquareArea(input_square); // рассчёт площади квадрата
            double circle_area = Calculation.CalculateCircleArea(input_circle); // рассчёт площади круга

            Console.WriteLine($"\nПлощади фигур:\n Квадрат: {square_area}\n Круг: {circle_area}");

            // сопоставление типов фигур
            string get_comparison_result = Calculation.Compare(square_area, circle_area);

            // украшение, не влияющее на логику
            Console.WriteLine("\n\nДля продолжения расчётов нажмите любую клавишу \nДля выхода из программы нажмите Ctrl + C и следуйте инструкции в консоли");
            Console.ReadKey();
        }
    }
}
