﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LabJob1.Tests
{
    [TestClass()]
    public class CalculationTests
    {
        [TestMethod()]
        public void CalculateCircleAreaTest()
        {
            // объявили радиус круга
            double circle_radius = 3;

            // провели расчёт площади круга
            double get_circle_area = Calculation.CalculateCircleArea(circle_radius);

            //сравнили полученный результат с ожидаемым
            Assert.AreEqual(get_circle_area, 28, 274333882308138);
        }

        [TestMethod()]
        public void CalculateSquareAreaTest()
        {
            // объявили радиус круга
            double square_line = 4;

            // провели расчёт площади круга
            double get_square_area = Calculation.CalculateSquareArea(square_line);

            //сравнили полученный результат с ожидаемым
            Assert.AreEqual(get_square_area, 16);
        }

        [TestMethod()]
        public void CompareTestSquareAreaBiggerThanCircleArea()
        {
            // объявили площадь квадрата
            double square_area = 16;

            // объявили площадь круга
            double circle_area = 12;

            // объявили переменную для получения сообщения от метода сравнения
            string message = Calculation.Compare(square_area, circle_area);

            // сравнили полученный результат с ожидаемым
            Assert.IsTrue(message.Contains("Площадь квадрата больше площади круга на"));
        }

        [TestMethod()]
        public void CompareTestCircleAreaBiggerThanSquareArea()
        {
            // объявили площадь квадрата
            double square_area = 16;

            // объявили площадь круга
            double circle_area = 25;

            // объявили переменную для получения сообщения от метода сравнения
            string message = Calculation.Compare(square_area, circle_area);

            // сравнили полученный результат с ожидаемым
            Assert.IsTrue(message.Contains("Площадь круга больше площади квадрата на"));
        }
    }
}