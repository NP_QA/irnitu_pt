﻿using System;
using System.Text;

namespace Lab_Job_1_string
{
    class Program
    {
        static void Main(string[] args)
        {
           // украшение, не влияющее на логику
            Console.WriteLine($"\nПриветствую!\nПрограмма из серии 'Строки', вариант №1. \n\nВыполнена студентом группы ИСТб-21-2: \nПогораевым Никитой Петровичем\n");
            Console.Write("Что-бы продолжить работу нажмите любую клавишу...");
            Console.ReadKey();
            Console.Clear();

            // логическая часть
            Console.WriteLine("Введите строку:");
            string console_input = Console.ReadLine();
            StringBuilder new_string = new StringBuilder(console_input);
            for (int i = 0; i < console_input.Length - 1; i += 2) // перебор с интервалом два символа.
            {
                char c = new_string[i + 1]; // Вспомогательная переменная для обмена символами.
                new_string[i + 1] = new_string[i]; // переопределение индексов в строке
                new_string[i] = c;
            }
            Console.WriteLine(new_string.ToString());
            Console.WriteLine("\n\nДля выхода из программы нажмите любую клавишу и следуйте инструкции");
            Console.ReadKey();
        }
    }
}