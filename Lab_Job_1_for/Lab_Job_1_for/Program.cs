﻿using System;

namespace Lab_Job_1_for
{
    class Program
    {
        private static void Main(string[] args)
        {
            // украшение, не влияющее на логику
            Console.WriteLine($"\nПриветствую!\nПрограмма из серии 'Циклы', вариант №1. \n\nВыполнена студентом группы ИСТб-21-2: \nПогораевым Никитой Петровичем\n");
            Console.Write("Что-бы приступить к расчётам нажмите любую клавишу...");
            Console.ReadKey();
            Console.Clear();
            for (int i = 1; i < 10; i++)
            {
                Console.WriteLine(string.Format("{0}*{1}={2}",i, 7, 7 * i));
            }
            Console.ReadKey();
            Console.WriteLine("\nДля выхода из программы нажмите Ctrl + C и следуйте инструкции в консоли");
            Console.ReadKey();
        }
    }
}
